package lesson4;

public class Lesson4 {

    public  static void main(String[] args) {
    average();
    }
    // подсчет среднеарифметического в массиве
    public static void average() {
        int arr[] = new int[] {2, 40, 4, 4};
        int sum = 0;

        for ( int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        System.out.println(sum / arr.length);
    }
}
