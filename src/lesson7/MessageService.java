package lesson7;

public class MessageService {
    public static void main(String[] args) {
        Message message = new Message();
//        message.sendMessage();
//        message.getMessage();
        callMessenger(message);

        WhatsApp whatsApp = new WhatsApp();
//        whatsApp.sendMessage();
//        whatsApp.getMessage();
        callMessenger(whatsApp);

    }
    private static void callMessenger(Messenger messenger) {
    messenger.getMessage();
    messenger.sendMessage();
    }
}
