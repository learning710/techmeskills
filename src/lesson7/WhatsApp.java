package lesson7;

public class WhatsApp implements Messenger {

    @Override
    public void sendMessage() {
        System.out.println("Отправили сообщени в WhatsApp");
    }

    @Override
    public void getMessage() {
        System.out.println("Получили сообщени в WhatsApp");
    }
}
