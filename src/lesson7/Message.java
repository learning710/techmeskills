package lesson7;

public class Message implements Messenger {

    @Override
    public void sendMessage() {
        System.out.println("отправил сообщение");
    }

    @Override
    public void getMessage() {
        System.out.println("получил сообщение");
    }
}
