package lesson7.Animal;

import java.util.Date;

public abstract class Animal {
    protected int animalID;
    protected String name;
    protected Date date;

    public void setAnimalID(int animalID) {
        this.animalID = animalID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    public abstract void printInfo();
}
