package lesson7.Animal;

import java.util.Date;

public class AnimalService {
    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.setAnimalID(1);
        cat.setName("Barsik");
        Date date = new Date();
        cat.setDate(date);
        cat.setEyesColor("Green");

        Dog dog = new Dog();
        dog.setAnimalID(2);
        dog.setName("Tuzik");
        dog.setDate(date);
        dog.setWeight(22.2);

        Tiger tiger = new Tiger();
        tiger.setAnimalID(3);
        tiger.setName("Scur");
        tiger.setDate(date);
        tiger.setEyesColor("Brown");
        tiger.setEatenExplorers(3);


        callPrint(cat);
        callPrint(dog);
        callPrint(tiger);
    }
    private static void callPrint (Animal animal) {
        animal.printInfo();
    }
}
