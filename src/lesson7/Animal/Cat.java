package lesson7.Animal;

public class Cat extends Animal {
    public String eyesColor;

    public void setEyesColor(String eyesColor) {
        this.eyesColor = eyesColor;
    }

    public void printInfo() {
        System.out.println(animalID + " " + name + " " + date + " " + eyesColor);
    }
}
