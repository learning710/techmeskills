package lesson3;

import java.util.Random;

public class HW3 {

    public static void main(String[] args) {
        randomInt();
        task1_1();
        task1_2();
        task2();
        task3();
    }
    // Задание 1. Сделать два метода
    //а) метод sortMax в котором сортировать элементы одномерного массива по возрастанию
    //б) метод sortMin в котором сортировать элементы одномерного массива по убыванию

    private static int randomInt() {
        Random random = new Random();
        return random.nextInt(100);
    }

    private static void task1_1() {
        int arr1[] = new int[10];

        for (int index = 0; index < arr1.length; index++) {
            arr1[index] = randomInt();
        }

        for (int i = 0; i < arr1.length; i++) {

            for (int j = 0; j < arr1.length - 1; j++) {
                if (arr1[j] > arr1[j + 1]) {
                    int perm = arr1[j + 1];
                    arr1[j + 1] = arr1[j];
                    arr1[j] = perm;
                }
            }
        }
        for (int a : arr1) {
            System.out.println(a + " ");
        }
    }

    private static void task1_2() {
        int arr2[] = new int[10];

        for (int index = 0; index < arr2.length; index++) {
            arr2[index] = randomInt();
        }

        for (int i = 0; i < arr2.length; i++) {

            for (int j = 0; j < arr2.length - 1; j++) {
                if (arr2[j] < arr2[j + 1]) {
                    int perm = arr2[j + 1];
                    arr2[j + 1] = arr2[j];
                    arr2[j] = perm;
                }
            }
        }
        for (int b : arr2) {
            System.out.print(b + " ");
        }
    }

    // Задание 2. Сделать «глупую сортировку» одномерного массива

    private static void task2() {
        int arr3[] = new int[]{2, 4, 1, 6, 11, 8, 9, 7};
        int b = 0;
        int index = arr3.length - 1;

        for (int i = 0; i < arr3.length; i++) {
            b++;
// if (index != 0) {

            for (int j = 0; j < arr3.length - 1; j++) {
                b++;

                if (arr3[j] > arr3[j + 1]) {
                    int perm = arr3[j + 1];
                    arr3[j + 1] = arr3[j];
                    arr3[j] = perm;
                    break;
                }
//
//  if (j + 1 == index) {
//  index = j;
//  System.out.println("previous elem: " + arr3[j]);
//  System.out.println("last elem: " + arr3[j + 1]);
//  break;
//  }
            }

            System.out.println("index: " + index);
//}
        }
        for (int a : arr3) {
            System.out.println(a);
        }
        System.out.println(b);
    }

    //Задание 3. Найти минимум в двумерном массиве

    private static void task3() {
        int[][] arrNew = new int[][]{{12, 1, -5, 22}, {16, -48, 75, 2}, {32, -1, 0, 4}};
        int minArrNew = arrNew[0][0];

        for (int i = 0; i < arrNew.length; i++) {
            int minRow = maximum(arrNew[i]);

            if (minRow < minArrNew) {
                minArrNew = minRow;
            }

        }

        System.out.println(minArrNew);
    }

    private static int maximum(int[] arr) {
        int minValue = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < minValue)
                minValue = arr[i];
        }

        System.out.println("Минимум: " + minValue);
        return minValue;

    }
}