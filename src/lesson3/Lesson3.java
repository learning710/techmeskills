package lesson3;

import java.util.Scanner;

public class Lesson3 {
    public static void main(String[] args) {
        task1();
    }

    private static void task1() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите размерность массива");
        int[] arr = new int[scan.nextInt()];

        for (int i = 0; i < arr.length; i++) {
            System.out.println("Введите значение массива");
            arr[i] = scan.nextInt();
        }

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        //int[] arr = new int[]{3, 2, 1, 4, 5, 7};


        for(int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                //Сравниваем два элемента
                if (arr[j] < arr[j + 1]) {
                    //Перестановка
                    int tmp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = tmp;
                }
            }
        }

        for(int a : arr) {
            System.out.print(a + " ");
        }

    }
    private  static  void exampleLesson4InputMatrix() {

    }
}
