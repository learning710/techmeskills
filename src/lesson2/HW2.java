package lesson2;

import java.util.Random;
import java.util.Scanner;

public class HW2 {
    public static void main(String[] args) {
        task1();
        task2();
        generateArrayAndFindMin();
    }

    // Задание 1 Даны 3 целых числа. Найти количество положительных чисел в исходном наборе.
    public static void task1() {
        int[] num = new int[]{1, 3, -2};
        if (num[0] > 0) {
            System.out.println(num[0]);
        }
        if (num[1] > 0) {
            System.out.println(num[1]);
        }
        if (num[2] < 0) {
            System.out.println();
        }
    }

    // Задание 2. Три целых числа вводятся из консоли. Найти количество положительных и отрицательных чисел в исходном наборе.

    public static void task2() {
        int[] arr = new int[3];
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < arr.length; i++) {
            int index = i + 1;
            System.out.println("Введите целое число " + index + ":");
            arr[i] = scanner.nextInt();
        }

        findPosAndNeg(arr);
    }

    public static void findPosAndNeg(int arr[]) {
        int pos = 0;
        int neg = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 0) {
                pos++;
            } else if (arr[i] < 0) {
                neg++;
            }
        }

        System.out.println("Положительных: " + pos + " Отрицательных: " + neg);
    }

    //  Задание 3. Найти минимум в массиве
    public static void generateArrayAndFindMin() {
        int[] arr = new int[10];

        for (int index = 0; index < arr.length; index++) {
            Random random = new Random();
            arr[index] = random.nextInt(100);
            System.out.println(arr[index]);
        }

        findMinimum(arr);
    }

    private static void findMinimum(int[] arr) {
        int minValue = arr[0];
        int i = 0;

        for (int index = 1; index < arr.length; index++) {
            if (arr[index] < minValue) {
                minValue = arr[index];
                i = index;
            }
        }
        System.out.println("Минимум: " + minValue + " на позиции " + i);
    }
}
