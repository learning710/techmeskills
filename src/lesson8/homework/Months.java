package lesson8.homework;

public enum Months {
    JANUARY(1, "Январь"),
    FEBRUARY(2, "Февраль"),
    MARCH(3, "Март"),
    APRIL(4, "Апрель"),
    MAY(5, "Май"),
    JUNE(6, "Июнь"),
    JULY(7, "Июль"),
    AUGUST(8, "Август"),
    SEPTEMBER(9, "Сентябрь"),
    OCTOBER(10, "Октябрь"),
    NOVEMBER(11, "Ноябрь"),
    DECEMBER(12, "Декабрь");

    int numbOfMonth;

    String nameOfMonth;

    Months(int inputNumbOfMonth, String inputNameOfMonth) {
        numbOfMonth = inputNumbOfMonth;
        nameOfMonth = inputNameOfMonth;
    }

    public static String getNumbOfMonth(int parameter) {
        Months[] months = Months.values();

        for (int i = 0; i < months.length; i++) {
            if (months[i].numbOfMonth == parameter) {
                return months[i].nameOfMonth;
            }
        }
        return null;
    }
}
