package lesson8.homework;

public class CalculateService {
    public static void main(String[] args) {
        Circle circle = new Circle(2.1);
        System.out.println("Площадь круга: " + circle.Area());

        Rectangle rectangle = new Rectangle(2, 3.4);
        System.out.println("Площадь прямоугольника: " + rectangle.Area());
    }
}
