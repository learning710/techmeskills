package lesson8.homework;

public class Circle implements Shape {
    double radius;
    private double p = 3.14;

    public Circle(double radius){
        this.radius = radius;
    }

    @Override
    public double Area() {
        return radius * radius *p;
    }
}
