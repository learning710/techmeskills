package lesson8.homework;

public interface Shape {

    public double Area();
}
