package lesson8.homework;

public class Rectangle implements Shape {
    double width;
    double height;


    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }


    @Override
    public double Area() {
        return width * height;
    }
}
