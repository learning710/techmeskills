package lesson8.exmp3;

public enum Days {

    MONDAY(1, "Понедельник"),
    TUESDAY(2, "Вторник"),
    WEDNESDAY(3, "Среда"),
    THURSDAY(4, "Четврег"),
    FRIDAY(5, "Пятница"),
    SATURDAY(6, "Суббота"),
    SUNDAY(7, "Воскресенье");

    int numbOfDay;
    String nameOfDay;
    Days(int inputDayNumbers, String inputNameOfDay) {
        numbOfDay = inputDayNumbers;
        nameOfDay = inputNameOfDay;
    }

    public static String getValueOfDayByNumb(int parameterFS) {
        Days[] days = Days.values();

        for (int i = 0; i < days.length; i++) {
            if (days[i].numbOfDay == parameterFS) {
                return days[i].nameOfDay;
            }
        }
        return null;
    }
}