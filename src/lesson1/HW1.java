package lesson1;

public class HW1 {
    public static void main(String[] args) {
        //Task 1
        int numb1 = 2;
        int numb2 = 6;
    if (numb1 < numb2) {
        System.out.println(numb1);
    } else {
        System.out.println(numb2);
    }
        //Task 2
        int numb3 = 12;
        int numb4 = 55;
        System.out.println(numb3 + numb4);
        //Task 3
        int numb5 = 22;
        double numb6 = 0.77;
        float numb7 = 10.1f;
        System.out.println(numb5 * numb6 * numb7);
        //Task 4
        int numb8 = 54;
        int numb9 = 5;
        System.out.println(numb8 % numb9);
    }
}