package lesson1;

public class HW1_hard {
    public static void main(String[] args) {
        System.out.println(compare(1, 2));
        summ(4,5);
        System.out.println(multiplication());
        System.out.println(segmentation(23,11));
        System.out.println(summ2(1.1, 2.2));
    }

    // Task1
    public static Integer compare(Integer numb1, Integer numb2) {
        if (numb1 > numb2) {
            return numb2;
        } else {
            return numb1;
        }
    }

    // Task2
    public static void summ(Integer numb1, Integer numb2) {
        System.out.println(numb1 + numb2);
    }

    // Task3
    public static Double multiplication() {
        int numb1 = 2;
        double numb2 = 2.2;
        double numb3 = 0.1;
        return numb1 * numb2 * numb3;
    }

    // Task4
    public static Integer segmentation(Integer numb1, Integer numb2) {
        return numb1 % numb2;
    }

    // Task6
    public static Integer summ2(Double numb1, Double numb2) {
        return (int) Math.round(numb1 + numb2);
    }
}

