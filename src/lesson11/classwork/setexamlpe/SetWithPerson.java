package lesson11.classwork.setexamlpe;

import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class SetWithPerson {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Set<Person> persons = new LinkedHashSet<>();
        String stop = scanner.next();

        while (!stop.equals("stop")) {
            Person person = new Person(stop);
            persons.add(person);
            stop = scanner.next();
        }

        for (Person elem: persons) {
            System.out.println(elem.getName());
        }

    }
}