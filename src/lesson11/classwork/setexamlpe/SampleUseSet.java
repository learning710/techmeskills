package lesson11.classwork.setexamlpe;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SampleUseSet {
    public static void main(String[] args) {
        Set<String> collect = new HashSet<>();
        collect.add("Vanya");
        collect.add("Vanya");
        collect.add("Petya");

        for (String elem: collect) {
            System.out.println(elem);
        }

        Map<String, Integer> mapByString = new HashMap<>();
        mapByString.put("one", 1);
        mapByString.put("two", 2);
        mapByString.put("three", 3);
        System.out.println(mapByString.get("three"));

        Map<Integer, String> mapByInteger = new HashMap<>();
        mapByInteger.put(1, "one");
        mapByInteger.put(2, "two");
        mapByInteger.put(3, "three");
        System.out.println(mapByInteger.get(3));
    }
}
