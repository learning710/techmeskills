package lesson11.classwork;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AverageOfList {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        List<Integer> list = new ArrayList<>();

        String stop = scanner.next();

        while (!stop.equals("stop")) {
            try {
                list.add(Integer.valueOf(stop));
            } catch (Exception input) {
                System.out.println("Вы ввели некорректнное значение, программа прервалась! Введите заново");
            }
            stop = scanner.next();
        }

        int summaElem = 0;

        for (Integer elem: list){
            summaElem = summaElem + elem;
        }

        double result = summaElem / list.size();
        System.out.println(result);
    }
}