package lesson11.classwork.mapexample;

import java.util.Objects;

public class Person {
    String name;
    String phone;

    public String getName() {
        return name;
    }

    Person(String name,String phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getPhone(){
        return phone;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
