package lesson11.homework;

import java.util.ArrayList;
import java.util.List;

public class ListNames {
    public static void main(String[] args) {

        List<String> people = new ArrayList<>();
        //Add in list
        people.add("Tom");
        people.add("Alice");
        people.add("Kate");
        people.add("Sam");
        //add an element by index 1
        people.add(1, "Bob");

        System.out.println(people.get(1));//Show an elem on index 1

        people.set(1, "Robert"); //rename element 1

        System.out.printf("ArrayList has %d elements \n", people.size());

        for (String person : people) {
            System.out.println(person);
        }

        //Checking for elements
        if (people.contains("Tom")) {
            System.out.println("ArrayList contains Tom");
        }
        //delete some objects
        //concrete obj
        people.remove("Robert");
        //delete by index
        people.remove(0);

        Object[] peopleArray = people.toArray();
        for (Object person : peopleArray) {
            System.out.println(">>>>" + person);
        }
    }
}
