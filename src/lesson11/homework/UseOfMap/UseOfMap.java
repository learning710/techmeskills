package lesson11.homework.UseOfMap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class UseOfMap {
    public static void main(String[] args) {

        Map<Integer, String> states = new HashMap<>();
        states.put(1,"Germany");
        states.put(2,"Spain");
        states.put(4,"France");
        states.put(3,"Italy");
        //show an obj by index 2
        String first = states.get(2);
        System.out.println(first);
        //show all keys
        Set<Integer> keys = states.keySet();
        //show all values
        Collection<String> values = states.values();
        //change an elem
        states.replace(1, "Poland");
        //delete an elem by key 2
        states.remove(2);
        //enumeration elements
        for (Map.Entry<Integer, String> item: states.entrySet()){
            System.out.printf("Key: %d Value: %s \n", item.getKey(), item.getValue());
        }
        Map<String, Person> people = new HashMap<>();
        people.put("1240i54", new Person("Tom"));
        people.put("1564i55", new Person("Bill"));
        people.put("4540i56", new Person("Nick"));

        for (Map.Entry<String, Person> item: people.entrySet()){
            System.out.printf("Key: %s Value: %s \n", item.getKey(), item.getValue().getName());
        }
    }
}
