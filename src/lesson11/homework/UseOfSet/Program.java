package lesson11.homework.UseOfSet;

import java.util.HashSet;
import java.util.Set;

public class Program {
    public static void main(String[] args) {

        Set<String> states = new HashSet<String>();
        // add elements
        states.add("Germany");
        states.add("France");
        states.add("Italy");
        //add an element, that we have in collection
        boolean isAdded = states.add("Germany");
        System.out.println(isAdded); //false
        System.out.printf("Set contains %d elements \n" , states.size());

        for (String state: states) {
            System.out.println(state);
        }

        //delete an elem
        states.remove("Germany");
        //Hash table of obj Person
        HashSet<Person> people = new HashSet<>();
        people.add(new Person("Mike"));
        people.add(new Person("Tom"));
        people.add(new Person("Nick"));

        for (Person p: people) {
            System.out.println(p.getName());
        }
    }
}
