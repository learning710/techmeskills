package lesson5;

public class TestPerson {

    public static void main(String[] args) {
        //Заполнение значения через метод
        Person deliver = new Person();
        deliver.setHight(175);
        System.out.println(deliver.getHight());

        //Заполнение значения через конструктор
        Person deliver2 = new Person(170);
        System.out.println(deliver2.getHight());
    }

}
