package lesson5;

public class StudentCalculation {
    public static void main(String[] args) {
        Student[] students = new Student[3];

        for (int i = 0; i < students.length; i++) {
            Student student = new Student();
            switch (i) {
                case 0:
                    student.setName("Маша");
                    student.setGroup(1);
                    student.setGrade(i + 3);

                case 1:
                    student.setName("Иван");
                    student.setGroup(1);
                    student.setGrade(i + 3);

                case 2:
                    student.setName("Алексей");
                    student.setGroup(1);
                    student.setGrade(i + 3);
            }

                    students[i] = student;
            }

        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].toString());
        }

    }
}