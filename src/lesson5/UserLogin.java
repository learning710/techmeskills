package lesson5;

public class UserLogin {
    public static void main(String[] args) {
        User[] users = new User[3];

        for (int i = 0; i < users.length; i++) {
            User user = new User();
            switch (i) {
                case 0:
                    user.setName("Nikolay");
                    user.setLogin("NikolayTr");
                    break;
                case 1:
                    user.setName("SomeoneName");
                    user.setLogin("SomeoneLogin");
                    break;
                default:
                    user.setName("DefaultName");
                    user.setLogin("DefaultLogin");
            }

                    users[i] = user;
            }

        for (int i = 0; i < users.length; i++) {
            System.out.println(users[i].toString());
        }
    }
}
