package lesson9.homework;

public class Circle implements Shape {
    double radius;
    double p = 3.14;
    public Circle(double radius) {
        this.radius = radius;
    }


    @Override
    public double Perimeter() {
        return 2 * radius * p;
    }

    @Override
    public double Area() {
        return p * radius * radius;
    }
}
