package lesson9.homework;

public interface Shape {
    public double Perimeter();
    public double Area();
}
