package lesson9.homework;

import java.util.InputMismatchException;
import java.util.Scanner;

public class CalculationForShapes {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

    //Круг

        System.out.println("Введите радиус круга");
        try {
            double inputRadius = scanner.nextDouble();
            Circle circle = new Circle(inputRadius);
            System.out.println("Площадь круга равна: " + circle.Area());
            System.out.println("Периметр круга равен: " + circle.Perimeter());

    //Прямоугольник

            System.out.println("Введите ширину прямоугольника");
            double inputWidth = scanner.nextDouble();
            System.out.println("Введите высоту прямоугольника");
            double inputHeight = scanner.nextDouble();
            Rectangle rectangle = new Rectangle(inputWidth, inputHeight);
            System.out.println("Площадь прямоугольника равна: " + rectangle.Area());
            System.out.println("Периметр прямоугольника равен: " + rectangle.Perimeter());
        } catch (Throwable error) {
            System.out.println("Вы ввели что-то не от " + error.fillInStackTrace());
        } finally {
            System.out.println("Запустите программу заново");
        }

    }
}
