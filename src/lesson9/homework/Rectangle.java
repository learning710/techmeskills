package lesson9.homework;

public class Rectangle implements Shape {
    double width;
    double height;

    public Rectangle(double width, double height){
        this.width = width;
        this.height = height;
    }

    @Override
    public double Perimeter() {
        return 2 * width + 2 * height;
    }

    @Override
    public double Area() {
        return width * height;
    }
}
