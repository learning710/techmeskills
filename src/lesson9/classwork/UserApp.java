package lesson9.classwork;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserApp {
    public static void main(String[] args) {

        User user = new User();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите Имя");
        String inputName = scanner.next();
        System.out.println("Считано: " + inputName);

        System.out.println("Введите фамилию");
        String inputSurname = scanner.next();
        System.out.println("Считано: " + inputSurname);

        try {
            user.name(inputName);
        } catch (InputMismatchException error1) {
            System.out.println("Введите корректное значение имени");
            user.name(scanner.next());;
        }

        try {
            user.surname(inputSurname);
        } catch (InputMismatchException error2) {
            System.out.println("Введите корректное значение фамилии");
            user.surname(scanner.next());
        }
    }
}
