package lesson9.classwork;

import java.util.InputMismatchException;

public class User {
    String name;
    String surname;

    // Для исключения не циферных значений
//        Integer numbers = Integer.parseInt(name);
//
//        if(numbers == null)
//          System.out.println("Корректно");
    public String name(String name) {

        if     (name.contains("1") ||
                name.contains("2") ||
                name.contains("3") ||
                name.contains("4") ||
                name.contains("5") ||
                name.contains("6") ||
                name.contains("7") ||
                name.contains("8") ||
                name.contains("9") ||
                name.contains("0")) {
            System.out.println("Имя введено неверно");
            throw new InputMismatchException();
        }
        this.name = name;
        return this.name;
    }

    public String surname(String surname) {

        if     (surname.contains("1") ||
                surname.contains("2") ||
                surname.contains("3") ||
                surname.contains("4") ||
                surname.contains("5") ||
                surname.contains("6") ||
                surname.contains("7") ||
                surname.contains("8") ||
                surname.contains("9") ||
                surname.contains("0")) {
            System.out.println("Фамилия введена неверно");
            throw new InputMismatchException();
        }
        this.surname = surname;
        return  this.surname;
    }
}
