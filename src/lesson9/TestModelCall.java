package lesson9;

public class TestModelCall {
    public static void main(String[] args) {

        String name = "test";
        String pref = "Журналист";
        TestModel testModel = new TestModel();
        System.out.println(testModel.getConstName());
        System.out.println(testModel.setName(name));
        testModel.displayInfo(pref);
        TestModel testModel1 = new TestModel();
        testModel1.name = "Vova";
        System.out.println(testModel1.name);
    }
}
