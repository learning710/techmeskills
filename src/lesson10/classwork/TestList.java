package lesson10.classwork;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestList {
    public static void main(String[] args) {
        List<Integer> numbers = new LinkedList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);

        int maxValue = numbers.get(0);
        for (Integer number: numbers){
            if (number > maxValue)
                maxValue = number;
        }
        System.out.println(maxValue);
        }
    }

