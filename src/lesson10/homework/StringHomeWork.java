package lesson10.homework;

import java.util.Scanner;

public class StringHomeWork {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = 3;

        System.out.println("Введите значения:");

        StringBuilder[] arrString = new StringBuilder[n];
        int[] lengthString = new int[n];

        for (int i = 0; i < arrString.length; i++) {
            arrString[i] = new StringBuilder(scanner.next());
            lengthString[i] = arrString[i].length();
        }
        double average;

        average = (lengthString[0] + lengthString[1] + lengthString[2])  / n;
        System.out.println("Среднее значение длины строк: " + average);
    }
}
