package lesson10.homework.practice;

public class Practice {
    public static void main(String[] args) {

        PrModel prModel = new PrModel();// Сюда можем задать значение из конструктора PrModel() {} в классе PrModel;

        //Передаем значение
        prModel.getString("Name");// Без ввода в переменную
        String test = prModel.getString("Name2"); // Через ввод в перменную

        //Присвоили значению value номер "1"
        prModel.setTestValue(1);

        // Возращаем значение из PrModel
        int value = prModel.getTestValue();

        //Задаем значение prModel1 отсюда PrModel(int testValue) {this.testValue = testValue;}
        PrModel prModel1 = new PrModel(1);

    }
}
