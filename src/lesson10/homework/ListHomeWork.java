package lesson10.homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ListHomeWork {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = 3;
        List<Integer> numbers = new ArrayList<>();

        System.out.println("Введите: ");
        int[] lengthString = new int[n];

        for (int i = 0; i < n; i++) {
            numbers.add(Integer.valueOf(scanner.next()));
            lengthString[i] = numbers.get(i);
        }

        int minValue = numbers.get(0);

        for (Integer number : numbers) {
            if (number < minValue)
                minValue = number;
        }
        System.out.println("Минимум: " + minValue);
    }
}